# more about the dataset here https://curate.nd.edu/show/0r967368551
# v299 has a malformatted q and a key

import pandas
import requests
import re

print('\nstarting survey download. this will take a minute or two.')
survey_responses_url = 'https://curate.nd.edu/downloads/fn106w94g79'
original_responses = pandas.read_csv(survey_responses_url) # takes a min!

survey_questions_url = 'https://curate.nd.edu/downloads/0v838051f6x'
survey_questions = requests.get(survey_questions_url).text
print('finished downloading! 📩')

def create_survey_key(survey_questions):
    survey_key = {}
    question_regex = re.compile('^(v\d+)\\t(.*)')
    answer_regex = re.compile('^\((\d+)\)\s(.*)')
    for line in survey_questions.splitlines():
        line = line.strip()
        question = re.match(question_regex, line)
        answer = re.match(answer_regex, line)
        if question:
            question_id = question.group(1)
            question_text = question.group(2)
            survey_key[question_id] = { 'text': '', 'answers': {} }
            survey_key[question_id]['text'] = question_text
        elif answer:
            answer_id = answer.group(1)
            answer_text = answer.group(2)
            survey_key[question_id]['answers'][answer_id] = answer_text
    return survey_key

def find_the_most_annoying_question(survey, survey_key):
    print('\nfinding the most annoying question. 🙄 🙄 🙄')
    tracker = {}
    for i, question in enumerate(survey):
        heart = bytes('\\U0001f49' + chr(i % 3 + 97), 'ascii')
        heart = heart.decode('unicode-escape')
        print('  checking question ' + str(i + 1) + '...  ' + heart)
        if question in ['sister', 'year']:
            pass
        else:
            folks_annoyed = 0
            for answer in survey[question]:
                text = get_answer_text(answer, question, survey_key)
                if 'so annoying' in text:
                    folks_annoyed += 1
            tracker[question] = folks_annoyed
    question, count = sorted(tracker.items(), key=lambda x: x[1])[-1]
    most_annoying_question = {
     'id': question[1:],
     'count': count,
     'text': survey_key[question]['text']
    }
    print('found it! 🔍')
    return most_annoying_question

def get_answer_text(answer, question, survey_key):
    try:
        answer_id = str(int(answer))
        text = survey_key[question]['answers'][answer_id]
    except ValueError:
        return '- missing response -'
    except KeyError:
        return '- missing answer key -'
    else:
        return text

survey_key = create_survey_key(survey_questions)
survey = original_responses.copy() # slice to work with a smaller set

maq = find_the_most_annoying_question(survey, survey_key)
print('\nthe most annoying question was #' + maq['id'] + '.')
print('\nit asked participants to respond to the following statement:')
print('\t"' + maq['text'] + '."')
print('\n' + str(maq['count']) + ' sisters found this annoying! 🙅 \n')
