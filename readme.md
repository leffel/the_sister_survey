## the Sister Survey ⛪️
read my blog post about working with this dataset [here.](http://bit.ly/an-annoying-question)

## what's going on here?

in 1967, Sister Marie Augusta Neal, SND, administered a 649 question survey to nearly 140,000 American women in Catholic ministry.

this repo intends to make the 'Sister Survey' more readily accessible for hobbyist analysis. you can access the dataset  [here](https://curate.nd.edu/show/0r967368551).

i've added a lighthearted intro script to provide a quick example. the script will find the question for which the most sisters responded that "The statement is so annoying to me that I cannot answer."

i've also included an iPython notebook that also does the above, plus additionally charts responses to a few demographic questions in the survey.

## i don't understand the most annoying question!
you're not alone, it is super confusing!

thankfully, [Eyebrows McGee](https://www.metafilter.com/user/102200) of MetaFilter has provided a thorough and engaging breakdown of the religious and historical context for the question. i encourage you to [read it here.](https://www.metafilter.com/170789/1967s-most-annoying-question-for-women-in-Catholic-ministry)

## okay, but tell me more about the Sister Survey.

> The survey was mailed to 157,917 sisters in 398 orders, and 89 percent of recipients responded to the questionnaire, which asked the sisters for their individual opinion on topics such as theology, social changes and the Vietnam War and led to detailed analysis of religious life at the time.

>Though Neal published her findings from the survey and follow-up work for decades afterward, the data itself — originally encoded on computer punch cards then stored on computer tapes — had been inaccessible to other researchers. But on Oct. 6, the University of Notre Dame announced that Neal's data is now available for researchers around the world to analyze, thanks to a university project.

you can read the above article [here](http://globalsistersreport.org/news/trends/unlocked-1967-survey-140000-us-sisters-now-accessible-all-49946).
